package com.company;

import java.io.IOException;
import java.net.Socket;

public class Client {

    public Client(String address, String fileName) throws IOException {
        String[] str = address.split(":");
        Socket s = new Socket(str[0], Integer.parseInt(str[1]));
        Session session = new Session(s, Protocol.CLIENT, new Board(fileName));
        new Thread(session, "BattleshipsClient").start();
    }
}
