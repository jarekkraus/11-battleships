package com.company;

import java.io.IOException;
import java.net.InetAddress;

import static com.company.SrvUtil.findAddress;

public class Main {

    public static void main(String[] args) throws IOException {
        if(args[0].equals("server")){
            Server server = new Server(findAddress(), Integer.parseInt(args[1]), args[2]);
            new Thread(server, "BattleshipsServer").start();
        }else
            new Client(args[1], args[2]);
    }
}
