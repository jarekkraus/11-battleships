package com.company;

import java.io.*;
import java.net.Socket;
import java.util.Random;

public class Session implements Runnable {

    private final Socket socket;
    private final Protocol mode;
    private final BufferedWriter out;
    private final BufferedReader in;
    private Board board;
    private String message;
    private String attackPosition;

    public Session(Socket socket, Protocol mode, Board board) throws IOException {
        this.socket = socket;
        this.mode = mode;
        this.board = board;
        this.message = "";

        out = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
        in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

        board.printMap();
    }

    @Override
    public void run() {
        try {
            if (mode == Protocol.CLIENT) {
                message = "start";
                send();
            }
            while (true) {
                String inputLine = in.readLine();
                if (!inputLine.isEmpty()) {
                    if (inputLine.contains("ostatni zatopiony")) {
                        lastSunk();
                    } else {
                        messageProcessing(inputLine);
                        send();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void messageProcessing(String inputLine) {
        System.out.println(inputLine);
        String[] str = inputLine.split(";");
        String command = str[0];
        String positionToAttack = str[1];
        message = board.attackAt(positionToAttack);
        updateOponnentMap(command);
        if (board.gameOver()) {
            System.out.println("ostatni zatopiony\nPrzegrana\n");
            board.printOpponentMapAfterLosing();
            System.out.println();
            board.printMap();
        }
    }
    private void lastSunk() {
        System.out.println("Wygrana\n");
        updateOponnentMap("trafiony");
        board.printOpponentMapAfterWinning();
        System.out.println();
        board.printMap();
        System.exit(0);
    }
    private void updateOponnentMap(String command) {
        switch (Command.valueOf(command.toUpperCase())) {
            case PUDŁO:
                board.updateOpponentMap('.', attackPosition);
                break;
            case TRAFIONY:
            case TRAFIONY_ZATOPIONY:
                board.updateOpponentMap('#', attackPosition);
                break;
        }
    }
    private String randomPosition() {
        Random rand = new Random();
        int random1 = rand.nextInt((74 - 65) + 1) + 65;
        int random2 = rand.nextInt((10 - 1) + 1) + 1;
        return Character.toString((char)random1)+random2;
    }
    private void send() throws IOException {
        String toSend;
        if (!board.gameOver()) {
            attackPosition = randomPosition();
            toSend = message + ";" + attackPosition;
        } else {
            toSend = "ostatni zatopiony\n";
        }
        out.write(toSend);
        out.newLine();
        out.flush();
    }
}
