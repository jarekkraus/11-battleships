package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class Board {
    private final int rows = 10;
    private final int cols = 10;
    public char[][] map;
    public char[][] opponentMap;
    HashSet<Ship> ships;
    HashSet<Ship> sunkShips;

    public Board(String fileName) {
        this.map = new char[rows][cols];
        this.opponentMap = new char[rows][cols];
        this.ships = new HashSet<>();
        this.sunkShips = new HashSet<>();
        var shipComponents = loadMapFromFile(fileName);
        createShips(shipComponents);
    }
    private ArrayList<Pair<Integer, Integer>> loadMapFromFile(String fileName) {
        ArrayList<Pair<Integer, Integer>> shipComponents = new ArrayList<>();
        try (var reader = new BufferedReader(new FileReader(new File(fileName)))) {
            String line;
            int i = 0;
            while ((line = reader.readLine()) != null) {
                for (int j=0; j<line.length(); j++) {
                    if (line.charAt(j) == '#') {
                        shipComponents.add(new Pair<>(i, j));
                    }
                    map[i][j] = line.charAt(j);
                    opponentMap[i][j] = '?';
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return shipComponents;
    }
    private Pair<Integer, Integer> getPosition(String position) {
        int col = position.charAt(0) - 65;
        int row = Integer.parseInt(position.substring(1)) - 1;
        return new Pair<>(row, col);
    }
    private Ship findShip(Pair<Integer, Integer> component) {
        Ship result = null;
        for (var ship : ships) {
            if (ship.contains(component)) {
                result = ship;
            }
        }
        return result;
    }

    public void createShips(ArrayList<Pair<Integer, Integer>> shipComponents) {
        for(var component : shipComponents){
            boolean check = true;
            for(var ship : ships) {
                if (ship.contains(component))
                    check = false;
            }
            if(check){
                Ship ship = new Ship(component);
                for(var p : shipComponents)
                    ship.addShipComponent(p);
                ships.add(ship);
            }
        }
    }
    public boolean gameOver() {
        return ships.size() == sunkShips.size();
    }
    public String attackAt(String position) {
        try {
            var positionInMap = getPosition(position);
            var positionToHit = map[positionInMap.x][positionInMap.y];
            if (positionToHit == '.' || positionToHit == '~') {
                map[positionInMap.x][positionInMap.y] = '~';
                return "pudło";
            } else {
                map[positionInMap.x][positionInMap.y] = '@';
                Ship ship = findShip(positionInMap);
                if (positionToHit == '#') {
                    ship.hit();
                }
                if (ship.isSunk()) {
                    sunkShips.add(ship);
                    return "trafiony_zatopiony";
                } else {
                    return "trafiony";
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return "poza planszą";
        }
    }
    public void updateOpponentMap(char newValue, String position){
        Pair<Integer, Integer> positionToUpdate = getPosition(position);
        opponentMap[positionToUpdate.x][positionToUpdate.y] = newValue;
    }

    public void printMap() {
        for (var line : map) {
            for (var el : line) {
                System.out.print(el);
            }
            System.out.println();
        }
    }
    public void printOpponentMap() {
        for (var line : opponentMap) {
            for (var el : line) {
                System.out.print(el);
            }
            System.out.println();
        }
    }
    public void printOpponentMapAfterLosing() {
        for (var ship : sunkShips) {
            for (var component : ship.getShipComponents()) {
                var x = component.x;
                var y = component.y;
                try {
                    if (opponentMap[x][y-1] == '?') opponentMap[x][y-1] = '.';
                    if (opponentMap[x][y+1] == '?') opponentMap[x][y-1] = '.';
                    if (opponentMap[x-1][y] == '?') opponentMap[x][y-1] = '.';
                    if (opponentMap[x+1][y] == '?') opponentMap[x][y-1] = '.';
                    if (opponentMap[x-1][y-1] == '?') opponentMap[x][y-1] = '.';
                    if (opponentMap[x+1][y-1] == '?') opponentMap[x][y-1] = '.';
                    if (opponentMap[x-1][y+1] == '?') opponentMap[x][y-1] = '.';
                    if (opponentMap[x+1][y+1] == '?') opponentMap[x][y-1] = '.';
                } catch (ArrayIndexOutOfBoundsException e) {}
            }
        }
        printOpponentMap();
    }
    public void printOpponentMapAfterWinning() {
        for(int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (opponentMap[i][j] == '?') {
                    opponentMap[i][j] = '.';
                }
            }
        }
        printOpponentMap();
    }
}
