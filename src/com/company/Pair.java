package com.company;

import java.util.Objects;

class Pair<U, V> {
    final U x;
    final V y;

    public Pair(U x, V y){
        this.x = x;
        this.y = y;
    }

    public boolean distanceOneUnit(Pair<Integer, Integer> a) {
        var deltaX = (Integer)x - a.x;
        var deltaY = (Integer)y - a.y;
        return Math.sqrt(deltaX*deltaX + deltaY*deltaY) <= Math.sqrt(2);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(x, pair.x) &&
                Objects.equals(y, pair.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "" + (char)((int)y + 65) + ((int)x + 1) + "   ";
    }
}
