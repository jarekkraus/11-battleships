package com.company;

import java.util.HashSet;

public class Ship {
    private HashSet<Pair<Integer, Integer>> shipComponents;
    private int lives;

    public Ship(Pair<Integer, Integer> shipComponent) {
        lives = 1;
        shipComponents = new HashSet<>();
        shipComponents.add(shipComponent);
    }

    public boolean contains(Pair<Integer, Integer> component) {
        return shipComponents.contains(component);
    }
    public void addShipComponent(Pair<Integer, Integer> shipComponent) {
        if (shipComponents.contains(shipComponent)) {
            return;
        }
        for (var component : shipComponents) {
            if (component.distanceOneUnit(shipComponent)) {
                shipComponents.add(shipComponent);
                lives += 1;
                break;
            }
        }
    }
    public void hit(){
        lives -= 1;
    }
    public boolean isSunk(){
        return lives <= 0;
    }
    public HashSet<Pair<Integer, Integer>> getShipComponents() {
        return shipComponents;
    }
}
