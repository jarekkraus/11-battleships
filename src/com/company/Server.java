package com.company;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {
    private final ServerSocket serverSocket;
    private String fileName;
    boolean killing = false;

    public Server(InetAddress address, int port, String fileName) throws IOException {
        serverSocket = new ServerSocket(port, 0, address);
        this.fileName = fileName;
        System.out.println("Running BattleShipsServer at address: " + address + ", port: " + port);
    }

    @Override
    public void run() {
        try {
            Socket socket = serverSocket.accept();
            System.out.println(killing);
            if (!killing) {
                new Thread(SrvUtil::doKill, "[KILLER]").start();
                killing = true;
            }
            System.out.println("Got request from " + socket.getRemoteSocketAddress() + ", starting session ");
            Session session = new Session(socket, Protocol.SERVER, new Board(fileName));
            new Thread(session, "SrvSession-" + 0).start();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
